package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"time"
)

func Handler(w http.ResponseWriter, req *http.Request) {

	time.Sleep(time.Duration(rand.Intn(800)+200) * time.Millisecond)

	w.Write([]byte("pong!"))
}

func main() {

	rand.Seed(time.Now().Unix())

	http.HandleFunc("/ping", Handler)

	if err := http.ListenAndServe(":8080", nil); err != nil {
		fmt.Println(err)
	}
}
