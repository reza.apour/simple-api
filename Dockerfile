FROM golang:1.20-alpine

WORKDIR /opt/api 

COPY go.mod . 

RUN go mod download 

RUN mkdir bin 

COPY . .  

RUN go build -o ./bin/app ./main.go 

CMD ["./bin/app"]
